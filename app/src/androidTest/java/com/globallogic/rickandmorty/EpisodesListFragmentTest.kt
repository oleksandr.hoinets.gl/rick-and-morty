package com.globallogic.rickandmorty

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import com.globallogic.rickandmorty.adapters.EpisodesViewHolder
import com.globallogic.rickandmorty.fragments.EpisodesListFragment
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.viewmodel.CharacterViewModel
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.EpisodeViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

@RunWith(AndroidJUnit4::class)
class EpisodesListFragmentTest {
    private lateinit var mockedRepository: Repository

    @Before
    fun setup() {
        mockedRepository = mockk()

        coEvery { mockedRepository.getEpisodes(any<Int>()) } returns (-1 to listOf(testEpisode1, testEpisode2))
        coEvery { mockedRepository.isFavorite(any<Episode>()) } returns true
        coEvery { mockedRepository.randomCharacters(listOf(testEpisode1, testEpisode2)) } returns listOf(
            testEpisode1,
            testEpisode2
        )
        coEvery { mockedRepository.isDownloaded(any()) } returns false
        coEvery { mockedRepository.getCharacters(listOf("1", "2")) } returns listOf(testCharacter1, testCharacter2)

        StandAloneContext.stopKoin()
        StandAloneContext.loadKoinModules(listOf(module {
            single { mockedRepository }
            viewModel { EpisodeViewModel(get(), Dispatchers.IO) }
            viewModel { FavoriteViewModel(get(), Dispatchers.IO) }
            viewModel { CharacterViewModel(get(), Dispatchers.IO) }
            viewModel { DownloadedViewModel(get(), Dispatchers.IO) }
        }))

        launchFragmentInContainer(themeResId = R.style.AppTheme) { EpisodesListFragment() }
    }

    @Test
    fun checkEpisodesList() {
        onView(atPositionOnView(R.id.rvEpisodesList, 0, R.id.tvEpisodeName))
            .check(matches(withText("Pilot")))
        onView(atPositionOnView(R.id.rvEpisodesList, 1, R.id.tvEpisodeName))
            .check(matches(withText("Pilot2")))
    }

    @Test
    fun changeFavoriteStatus() {
        onView(atPositionOnView(R.id.rvEpisodesList, 0, R.id.cbFavoriteEpisode)).perform(click())

        coVerify {
            mockedRepository.updateFavoriteStatus(testEpisode1)
            mockedRepository.getEpisodes(any<Int>())
            mockedRepository.randomCharacters(listOf(testEpisode1, testEpisode2))
            mockedRepository.toString()
        }

        coVerify(exactly = 2) {
            mockedRepository.isFavorite(any<Episode>())
        }

        confirmVerified(mockedRepository)
    }

    @Test
    fun openEpisodeInfoActivity() {
        val episode = Episode(1, "Pilot", "", "", listOf(), listOf("1", "2"), false)
        coEvery { mockedRepository.getEpisodes(any<Int>()) } returns (-1 to listOf(episode, testEpisode2))
        coEvery { mockedRepository.updateFavoriteStatus(episode) } answers { episode.isFavorite = !episode.isFavorite }
        coEvery { mockedRepository.isFavorite(episode) } returns episode.isFavorite

        onView(atPositionOnView(R.id.rvEpisodesList, 0, R.id.cbFavoriteEpisode))
            .check(matches(isChecked()))

        onView(withId(R.id.rvEpisodesList)).perform(actionOnItemAtPosition<EpisodesViewHolder>(0, click()))
        onView(withId(R.id.tvEpisodeNameInfoToolbar)).check(matches(withText("Pilot")))
        onView(withId(R.id.cbFavoriteEpisodeInfo)).perform(click())
        Espresso.pressBack()

        onView(atPositionOnView(R.id.rvEpisodesList, 0, R.id.cbFavoriteEpisode))
            .check(matches(isNotChecked()))
        onView(atPositionOnView(R.id.rvEpisodesList, 1, R.id.cbFavoriteEpisode))
            .check(matches(isChecked()))

        coVerify {
            mockedRepository.randomCharacters(listOf(testEpisode1, testEpisode2))
            mockedRepository.getEpisodes(any<Int>())
            mockedRepository.updateFavoriteStatus(any<Episode>())
            mockedRepository.getCharacters(listOf("1", "2"))
            mockedRepository.isDownloaded(any<Episode>())
            mockedRepository.toString()
            mockedRepository.isFavorite(testCharacter1)
            mockedRepository.isFavorite(testCharacter2)
        }

        coVerify(exactly = 3) { mockedRepository.isFavorite(testEpisode1) }
        coVerify(exactly = 2) { mockedRepository.isFavorite(testEpisode2) }

        confirmVerified(mockedRepository)
    }
}