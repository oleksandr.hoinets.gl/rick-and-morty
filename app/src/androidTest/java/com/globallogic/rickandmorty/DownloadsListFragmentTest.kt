package com.globallogic.rickandmorty

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.runner.AndroidJUnit4
import com.globallogic.rickandmorty.fragments.DownloadsFragment
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.EpisodeViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

@RunWith(AndroidJUnit4::class)
class DownloadsListFragmentTest {
    private lateinit var mockedRepository: Repository

    @Before
    fun setup() {
        mockedRepository = mockk()

        coEvery { mockedRepository.getAllCharacters(any()) } returns listOf(testCharacter1, testCharacter2)
        coEvery { mockedRepository.getAllEpisodes(any()) } returns listOf(testEpisode1, testEpisode2)
        coEvery { mockedRepository.isFavorite(any<Episode>()) } returns true
        coEvery { mockedRepository.isFavorite(any<Character>()) } returns true
        coEvery { mockedRepository.getEpisodes(listOf("1", "2")) } returns listOf(testEpisode1, testEpisode2)
        coEvery { mockedRepository.isDownloaded(any<Character>()) } returns true

        StandAloneContext.stopKoin()
        StandAloneContext.loadKoinModules(listOf(module {
            single { mockedRepository }
            viewModel { FavoriteViewModel(get(), Dispatchers.IO) }
            viewModel { DownloadedViewModel(get(), Dispatchers.IO) }
            viewModel { EpisodeViewModel(get(), Dispatchers.IO) }
        }))

        launchFragmentInContainer(themeResId = R.style.AppTheme) { DownloadsFragment() }
    }

    @Test
    fun checkItems() {
        onView(atPositionOnView(R.id.rvDownloads, 0, R.id.tvName))
            .check(matches(withText("Rick")))
        onView(atPositionOnView(R.id.rvDownloads, 2, R.id.tvEpisodeName))
            .check(matches(withText("Pilot")))

        coVerify {
            mockedRepository.getAllCharacters(any())
            mockedRepository.getAllEpisodes(any())
        }

        confirmVerified(mockedRepository)
    }

    @Test
    fun changeFavoriteStatus() {
        onView(atPositionOnView(R.id.rvDownloads, 1, R.id.cbFavoriteCharacter)).perform(click())
        onView(atPositionOnView(R.id.rvDownloads, 2, R.id.cbFavoriteEpisode)).perform(click())

        coVerify {
            mockedRepository.updateFavoriteStatus(any())
            mockedRepository.getAllCharacters(any())
            mockedRepository.getAllEpisodes(any())
            mockedRepository.toString()
        }

        confirmVerified(mockedRepository)
    }
}