package com.globallogic.rickandmorty

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import com.globallogic.rickandmorty.adapters.CharactersViewHolder
import com.globallogic.rickandmorty.fragments.CharactersListFragment
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Location
import com.globallogic.rickandmorty.common.model.Status
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.viewmodel.CharacterViewModel
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.EpisodeViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

@RunWith(AndroidJUnit4::class)
class CharactersListFragmentTest {
    private lateinit var mockedRepository: Repository

    @Before
    fun setup() {
        mockedRepository = mockk()

        coEvery { mockedRepository.getCharacters(any<Int>()) } returns (-1 to listOf(testCharacter1, testCharacter2))
        coEvery { mockedRepository.isFavorite(any<Character>()) } returns true
        coEvery { mockedRepository.isDownloaded(any()) } returns true
        coEvery { mockedRepository.getEpisodes(listOf("1", "2")) } returns listOf(testEpisode1, testEpisode2)
        coEvery { mockedRepository.randomCharacters(listOf(testEpisode1, testEpisode2)) } returns listOf(
            testEpisode1,
            testEpisode2
        )

        StandAloneContext.stopKoin()
        StandAloneContext.loadKoinModules(listOf(module {
            single { mockedRepository }
            viewModel { CharacterViewModel(get(), Dispatchers.IO) }
            viewModel { FavoriteViewModel(get(), Dispatchers.IO) }
            viewModel { EpisodeViewModel(get(), Dispatchers.IO) }
            viewModel { DownloadedViewModel(get(), Dispatchers.IO) }
        }))

        launchFragmentInContainer(themeResId = R.style.AppTheme) { CharactersListFragment() }
    }

    @Test
    fun checkItems() {
        onView(atPositionOnView(R.id.rvCharactersList, 0, R.id.tvName))
            .check(matches(withText("Rick")))
        onView(atPositionOnView(R.id.rvCharactersList, 1, R.id.tvName))
            .check(matches(withText("Morty")))

        coVerify { mockedRepository.getCharacters(any<Int>()) }

        confirmVerified(mockedRepository)
    }

    @Test
    fun changeFavoriteStatus() {
        onView(atPositionOnView(R.id.rvCharactersList, 0, R.id.cbFavoriteCharacter)).perform(click())

        coVerify {
            mockedRepository.updateFavoriteStatus(testCharacter1)
            mockedRepository.getCharacters(any<Int>())
            mockedRepository.toString()
        }

        confirmVerified(mockedRepository)
    }

    @Test
    fun openCharacterInfoActivity() {
        val character = Character(1, "Rick", Status.ALIVE, null, "", "", Location("Earth"), listOf("1", "2"), false)
        coEvery { mockedRepository.getCharacters(any<Int>()) } returns (-1 to listOf(character, testCharacter2))
        coEvery { mockedRepository.updateFavoriteStatus(character) } answers { character.isFavorite = !character.isFavorite }
        coEvery { mockedRepository.isFavorite(character) } returns character.isFavorite
        coEvery { mockedRepository.isFavorite(testCharacter2) } returns true

        onView(atPositionOnView(R.id.rvCharactersList, 0, R.id.cbFavoriteCharacter))
            .check(matches(isChecked()))

        onView(withId(R.id.rvCharactersList)).perform(actionOnItemAtPosition<CharactersViewHolder>(0, click()))
        onView(withId(R.id.tvCharacterNameToolbar)).check(matches(withText("Rick")))
        onView(withId(R.id.cbFavoriteCharacterInfo)).perform(click())
        Espresso.pressBack()

        onView(atPositionOnView(R.id.rvCharactersList, 0, R.id.cbFavoriteCharacter))
            .check(matches(not(isChecked())))
        onView(atPositionOnView(R.id.rvCharactersList, 1, R.id.cbFavoriteCharacter))
            .check(matches(isChecked()))

        coVerify {
            mockedRepository.getCharacters(any<Int>())
            mockedRepository.updateFavoriteStatus(any<Character>())
            mockedRepository.getEpisodes(listOf("1", "2"))
            mockedRepository.isDownloaded(any<Character>())
            mockedRepository.toString()
        }

        coVerify(exactly = 3) {
            mockedRepository.isFavorite(any<Character>())
        }

        confirmVerified(mockedRepository)
    }
}