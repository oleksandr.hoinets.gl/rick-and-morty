package com.globallogic.rickandmorty

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

fun atPositionOnView(recyclerViewId: Int, position: Int, targetViewId: Int) =
    object : TypeSafeMatcher<View>() {
        override fun describeTo(description: Description?) {
            description?.appendText("with recycler view $recyclerViewId at position $position")
        }

        override fun matchesSafely(view: View): Boolean {
            val childView: View
            val recyclerView = view.rootView.findViewById<RecyclerView>(recyclerViewId)
            if (recyclerView != null) {
                childView = recyclerView.findViewHolderForAdapterPosition(position)?.itemView ?: return false
            } else {
                return false
            }

            return view == if (targetViewId == -1) childView else childView.findViewById<View>(targetViewId)
        }
    }