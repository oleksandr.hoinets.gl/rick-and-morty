package com.globallogic.rickandmorty

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Location
import com.globallogic.rickandmorty.common.model.Status

val testCharacter1 = Character(1, "Rick", Status.ALIVE, null, "", "", Location("Earth"), listOf("1", "2"), true)
val testCharacter2 = Character(2, "Morty", Status.ALIVE, null, "", "", Location("Earth"), listOf("1", "2"), true)
val testEpisode1 = Episode(1, "Pilot", "", "", listOf(), listOf("1", "2"), true)
val testEpisode2 = Episode(2, "Pilot2", "", "", listOf(), listOf("1", "2"), true)