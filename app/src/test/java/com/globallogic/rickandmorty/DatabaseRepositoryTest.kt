package com.globallogic.rickandmorty

import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.database.*
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class DatabaseRepositoryTest {
    private lateinit var mockDatabase: AppDatabase
    private lateinit var repository: DatabaseRepository
    private lateinit var mockFavoriteEpisodesDao: FavoriteEpisodesDao
    private lateinit var mockFavoriteCharactersDao: FavoriteCharactersDao
    private lateinit var mockEpisodesDao: EpisodesDao
    private lateinit var mockCharactersDao: CharactersDao

    @Before
    fun setup() {
        mockDatabase = mockk(relaxed = true, relaxUnitFun = true)
        repository = DatabaseRepository(mockDatabase)

        mockFavoriteEpisodesDao = mockk()
        mockFavoriteCharactersDao = mockk()
        mockEpisodesDao = mockk()
        mockCharactersDao = mockk()
        every { mockDatabase.favoriteEpisodesDao() } returns mockFavoriteEpisodesDao
        every { mockDatabase.favoriteCharactersDao() } returns mockFavoriteCharactersDao
        every { mockDatabase.episodesDao() } returns mockEpisodesDao
        every { mockDatabase.charactersDao() } returns mockCharactersDao
    }

    @Test
    fun `is favorite`() {
        every { mockFavoriteEpisodesDao.getById(1) } returns null
        every { mockFavoriteCharactersDao.getById(1) } returns null
        val episodeFavoriteEntity = FavoriteEpisodesEntity(episode2.id, 5, true)
        every { mockFavoriteEpisodesDao.getById(2) } returns episodeFavoriteEntity
        val characterFavoriteEntity = FavoriteCharactersEntity(character2.id, 5, true)
        every { mockFavoriteCharactersDao.getById(2) } returns characterFavoriteEntity

        runBlocking {
            assertFalse(repository.isFavorite(episode1))
            assertTrue(repository.isFavorite(episode2))
            assertFalse(repository.isFavorite(character1))
            assertTrue(repository.isFavorite(character2))
        }

        coVerify {
            mockFavoriteEpisodesDao.getById(1)
            mockFavoriteEpisodesDao.getById(2)
            mockFavoriteCharactersDao.getById(1)
            mockFavoriteCharactersDao.getById(2)
        }

        confirmVerified(mockFavoriteEpisodesDao, mockFavoriteCharactersDao)
    }

    @Test
    fun `error getting favorite status`() {
        assertExceptionThrown { repository.isFavorite(xyilo) }
    }

    @Test
    fun `insert to download`() {
        val episodeEntity = episode1.toEntity()
        val characterEntity = character1.toEntity()

        every { mockEpisodesDao.insert(any()) } just runs
        every { mockCharactersDao.insert(any()) } just runs

        runBlocking {
            repository.insertToDownload(episode1)
            repository.insertToDownload(character1)
        }

        coVerify {
            mockEpisodesDao.insert(match { it.id == episodeEntity.id })
            mockCharactersDao.insert(match { it.id == characterEntity.id })
        }

        confirmVerified(mockEpisodesDao, mockCharactersDao)
    }

    @Test
    fun `error inserting to download`() {
        assertExceptionThrown { repository.insertToDownload(xyilo) }
    }

    @Test
    fun `delete from download`() {
        val episodeEntity = episode1.toEntity()
        val characterEntity = character1.toEntity()

        every { mockEpisodesDao.delete(any()) } just runs
        every { mockCharactersDao.delete(any()) } just runs

        runBlocking {
            repository.deleteFromDownload(episode1)
            repository.deleteFromDownload(character1)
        }

        coVerify {
            mockEpisodesDao.delete(match { it.id == episodeEntity.id })
            mockCharactersDao.delete(match { it.id == characterEntity.id })
        }

        confirmVerified(mockEpisodesDao, mockCharactersDao)
    }

    @Test
    fun `error deleting from download`() {
        assertExceptionThrown { repository.deleteFromDownload(xyilo) }
    }

    @Test
    fun `update favorite status`() {
        val episodeFavoriteEntity = FavoriteEpisodesEntity(1, 5, true)
        every { mockFavoriteEpisodesDao.getById(1) } returns episodeFavoriteEntity
        val characterFavoriteEntity = FavoriteCharactersEntity(1, 5, true)
        every { mockFavoriteCharactersDao.getById(1) } returns characterFavoriteEntity
        every { mockFavoriteEpisodesDao.insert(any()) } just runs
        every { mockFavoriteCharactersDao.insert(any()) } just runs

        runBlocking {
            repository.updateFavoriteStatus(episode1)
            repository.updateFavoriteStatus(character1)
        }

        coVerify {
            mockFavoriteEpisodesDao.getById(1)
            mockFavoriteEpisodesDao.insert(match { it.id == episodeFavoriteEntity.id })
            mockFavoriteCharactersDao.getById(1)
            mockFavoriteCharactersDao.insert(match { it.id == characterFavoriteEntity.id })
        }

        confirmVerified(mockFavoriteEpisodesDao, mockFavoriteCharactersDao)
    }

    @Test
    fun `error updating favorite status`() {
        assertExceptionThrown { repository.updateFavoriteStatus(xyilo) }
    }

    @Test
    fun `is downloaded`() {
        every { mockCharactersDao.getById(1) } returns null
        every { mockEpisodesDao.getById(1) } returns null
        val charactersEntity = character2.toEntity()
        every { mockCharactersDao.getById(2) } returns charactersEntity
        val episodeEntity = episode2.toEntity()
        every { mockEpisodesDao.getById(2) } returns episodeEntity

        runBlocking {
            assertFalse(repository.isDownloaded(character1))
            assertFalse(repository.isDownloaded(episode1))
            assertTrue(repository.isDownloaded(character2))
            assertTrue(repository.isDownloaded(episode2))
        }

        coVerify {
            mockCharactersDao.getById(1)
            mockCharactersDao.getById(2)
            mockEpisodesDao.getById(1)
            mockEpisodesDao.getById(2)
        }

        confirmVerified(mockCharactersDao, mockEpisodesDao)
    }

    @Test
    fun `error getting downloaded status`() {
        assertExceptionThrown { repository.isDownloaded(xyilo) }
    }

    @Test
    fun `get all characters`() {
        val characters = listOf(character1.toEntity(), character2.toEntity())
        every { mockCharactersDao.getAllCharacters() } returns characters

        runBlocking { assertEquals(listOf(character1, character2), repository.getAllCharacters()) }

        coVerify { mockCharactersDao.getAllCharacters() }

        confirmVerified(mockCharactersDao)
    }

    @Test
    fun `get all episodes`() {
        val episodes = listOf(episode1.toEntity(), episode2.toEntity())
        every { mockEpisodesDao.getAllEpisodes() } returns episodes
        every { mockCharactersDao.getById(any()) } returns null

        runBlocking { assertEquals(listOf(episode1, episode2), repository.getAllEpisodes()) }

        coVerify { mockEpisodesDao.getAllEpisodes() }

        confirmVerified(mockEpisodesDao)
    }

    @Test
    fun `search characters`() {
        val characters20 = List(20) { character1.toEntity() }
        val characters2 = listOf(character1.toEntity(), character2.toEntity())
        every { mockCharactersDao.findByName("%r%") } returns characters2
        every { mockCharactersDao.findByName("%k%") } returns characters20

        runBlocking {
            assertEquals(listOf(character1, character2), repository.searchCharacters("r"))
            assertEquals(10, repository.searchCharacters("k").size)
        }

        coVerify {
            mockCharactersDao.findByName("%r%")
            mockCharactersDao.findByName("%k%")
        }

        confirmVerified(mockCharactersDao)
    }

    @Test
    fun `get characters`() {
        val ids = listOf("1", "3", "2")
        val characterEntity1 = character1.toEntity()
        every { mockCharactersDao.getById(1) } returns characterEntity1
        val characterEntity2 = character2.toEntity()
        every { mockCharactersDao.getById(2) } returns characterEntity2
        every { mockCharactersDao.getById(3) } returns null

        runBlocking {
            assertEquals(listOf(character1, character2), repository.getCharacters(ids))
        }

        coVerify {
            mockCharactersDao.getById(1)
            mockCharactersDao.getById(2)
            mockCharactersDao.getById(3)
        }

        confirmVerified(mockCharactersDao)
    }

    @Test
    fun `random characters`() {
        val episodes = listOf(episode1, episode2)

        val expectedResult = listOf(
            Episode(1, "Pilot", "", "", listOf(character1, character2), listOf("1", "2"), false),
            Episode(2, "Pilot2", "", "", listOf(character1, character2), listOf("1", "2"), false)
        )
        every { mockCharactersDao.getById(1) } returns character1.toEntity()
        every { mockCharactersDao.getById(2) } returns character2.toEntity()

        runBlocking { assertEquals(expectedResult, repository.randomCharacters(episodes)) }

        coVerify {
            mockCharactersDao.getById(1)
            mockCharactersDao.getById(2)
        }

        confirmVerified(mockCharactersDao)
    }

    private fun <T> assertExceptionThrown(block: suspend () -> T) {
        runBlocking {
            var exception: Exception? = null
            try {
                block()
            } catch (e: Exception) {
                exception = e
            }
            assertNotNull(exception)
        }
    }
}