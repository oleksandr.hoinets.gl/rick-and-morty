package com.globallogic.rickandmorty

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Location
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FavoriteViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var mockRepository: Repository
    private lateinit var viewModel: FavoriteViewModel
    private val mockCharacterObserver = mockk<Observer<Character>>(relaxUnitFun = true, relaxed = true)
    private val mockEpisodesObserver = mockk<Observer<List<Episode>>>(relaxUnitFun = true, relaxed = true)

    @Before
    fun setup() {
        mockRepository = mockk()
        viewModel = FavoriteViewModel(mockRepository, Dispatchers.Unconfined)
    }

    @Test
    fun `fetch favorite character status`() {
        val character = Character(1, "", null, null, "", "", Location(""), listOf(), true)
        coEvery { mockRepository.isFavorite(character) } returns true

        viewModel.character.observeForever(mockCharacterObserver)
        viewModel.fetchCharacterFavoriteStatus(character)

        verify { mockCharacterObserver.onChanged(character) }
        coVerify { mockRepository.isFavorite(character) }

        confirmVerified(mockRepository, mockCharacterObserver)
    }

    @Test
    fun `fetch favorite episodes status`() {
        val episodes = listOf(
            Episode(1, "Pilot", "", "", listOf(), listOf("1", "2"), false),
            Episode(2, "Pilot2", "", "", listOf(), listOf("1", "2"), false),
            Episode(3, "Pilot3", "", "", listOf(), listOf("1", "2"), false)
        )

        coEvery { mockRepository.isFavorite(episodes[0]) } returns true
        coEvery { mockRepository.isFavorite(episodes[1]) } returns false
        coEvery { mockRepository.isFavorite(episodes[2]) } returns true

        viewModel.episodes.observeForever(mockEpisodesObserver)
        viewModel.fetchEpisodesFavoriteStatus(episodes)

        verify { mockEpisodesObserver.onChanged(episodes) }
        coVerify { mockRepository.isFavorite(episodes[0]) }
        coVerify { mockRepository.isFavorite(episodes[1]) }
        coVerify { mockRepository.isFavorite(episodes[2]) }

        confirmVerified(mockRepository, mockEpisodesObserver)
    }
}