package com.globallogic.rickandmorty

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Favorite
import com.globallogic.rickandmorty.common.model.Location

data class Xyilo(override var isFavorite: Boolean) : Favorite

val character1 = Character(1, "Rick", null, null, "", "", Location(""), listOf(), false)
val character2 = Character(2, "Morty", null, null, "", "", Location(""), listOf(), false)
val episode1 = Episode(1, "Pilot", "", "", listOf(), listOf("1", "2"), false)
val episode2 = Episode(2, "Pilot2", "", "", listOf(), listOf("1", "2"), false)

val xyilo = Xyilo(true)
