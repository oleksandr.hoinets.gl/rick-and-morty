package com.globallogic.rickandmorty

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.globallogic.rickandmorty.common.model.SearchHeader
import com.globallogic.rickandmorty.common.model.SearchNoResults
import com.globallogic.rickandmorty.common.model.SearchSeparator
import com.globallogic.rickandmorty.common.model.SwitchToSourceButton
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.viewmodel.SearchViewModel
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SearchViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var mockRepository: Repository
    private lateinit var viewModel: SearchViewModel
    private val mockSearchResultsObserver = mockk<Observer<List<Any>>>(relaxUnitFun = true, relaxed = true)

    @Before
    fun setup() {
        mockRepository = mockk(relaxUnitFun = true, relaxed = true)
        viewModel = SearchViewModel(mockRepository, Dispatchers.Unconfined)
    }

    @Test
    fun `toggle online`() {
        coEvery { mockRepository.searchCharacters(any(), any()) } returns listOf()
        coEvery { mockRepository.searchEpisodes(any(), any()) } returns listOf()

        viewModel.search("1234")

        coVerify {
            mockRepository.searchCharacters("1234", true)
            mockRepository.searchEpisodes("1234", true)
        }
        viewModel.toggleOnline()
        coVerify {
            mockRepository.searchCharacters("1234", false)
            mockRepository.searchEpisodes("1234", false)
        }

        confirmVerified(mockRepository)
    }

    @Test
    fun `search results only characters offline`() {
        coEvery { mockRepository.searchCharacters(any(), true) } returns listOf(character1, character2)
        coEvery { mockRepository.searchEpisodes(any(), any()) } returns listOf()

        viewModel.searchResults.observeForever(mockSearchResultsObserver)
        viewModel.search("rick")

        coVerify {
            mockRepository.searchCharacters("rick", true)
            mockRepository.searchEpisodes(any(), any())
        }

        verify {
            mockSearchResultsObserver.onChanged(
                listOf(
                    SearchHeader(R.string.characters),
                    character1,
                    character2,
                    SearchSeparator(),
                    SwitchToSourceButton(R.string.load_online_results)
                )
            )
        }

        confirmVerified(mockRepository, mockSearchResultsObserver)
    }

    @Test
    fun `search results characters and episodes online`() {
        coEvery { mockRepository.searchCharacters(any(), any()) } returns listOf(character1, character2)
        coEvery { mockRepository.searchEpisodes(any(), any()) } returns listOf(episode1, episode2)

        viewModel.searchResults.observeForever(mockSearchResultsObserver)
        viewModel.search("1")
        viewModel.toggleOnline()

        coVerify(ordering = Ordering.ORDERED) {
            mockRepository.searchCharacters("1", true)
            mockRepository.searchEpisodes("1", true)
            mockRepository.searchCharacters("1", false)
            mockRepository.searchEpisodes("1", false)
        }

        verify(ordering = Ordering.ORDERED) {
            mockSearchResultsObserver.onChanged(any())
            mockSearchResultsObserver.onChanged(
                listOf(
                    SearchHeader(R.string.characters),
                    character1,
                    character2,
                    SearchSeparator(),
                    SearchHeader(R.string.episodes),
                    episode1,
                    episode2,
                    SearchSeparator(),
                    SwitchToSourceButton(R.string.load_offline_results)
                )
            )
        }

        confirmVerified(mockRepository, mockSearchResultsObserver)
    }

    @Test
    fun `search no results online`() {
        coEvery { mockRepository.searchCharacters(any(), any()) } returns listOf()
        coEvery { mockRepository.searchEpisodes(any(), any()) } returns listOf()

        viewModel.searchResults.observeForever(mockSearchResultsObserver)
        viewModel.search("1")
        viewModel.toggleOnline()

        coVerify(ordering = Ordering.ORDERED) {
            mockRepository.searchCharacters("1", true)
            mockRepository.searchEpisodes("1", true)
            mockRepository.searchCharacters("1", false)
            mockRepository.searchEpisodes("1", false)
        }

        verify(ordering = Ordering.ORDERED) {
            mockSearchResultsObserver.onChanged(any())
            mockSearchResultsObserver.onChanged(listOf(SearchNoResults()))
        }

        confirmVerified(mockRepository, mockSearchResultsObserver)
    }

    @Test
    fun `search no results offline`() {
        coEvery { mockRepository.searchCharacters(any(), true) } returns listOf()
        coEvery { mockRepository.searchEpisodes(any(), true) } returns listOf()

        viewModel.searchResults.observeForever(mockSearchResultsObserver)
        viewModel.search("1")

        coVerify {
            mockRepository.searchCharacters(any(), true)
            mockRepository.searchEpisodes(any(), true)
        }

        verify {
            mockSearchResultsObserver.onChanged(
                listOf(
                    SearchNoResults(),
                    SwitchToSourceButton(R.string.try_online_search)
                )
            )
        }

        confirmVerified(mockRepository, mockSearchResultsObserver)
    }
}