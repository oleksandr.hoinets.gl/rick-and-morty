package com.globallogic.rickandmorty

import com.globallogic.rickandmorty.common.model.*
import com.globallogic.rickandmorty.repository.network.NetworkRepository
import com.globallogic.rickandmorty.repository.network.RickAndMortyApi
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.lang.Exception

@Suppress("DeferredResultUnused")
class NetworkRepositoryTest {
    private lateinit var mockApi: RickAndMortyApi
    private lateinit var repository: NetworkRepository

    @Before
    fun setup() {
        mockApi = mockk(relaxed = true, relaxUnitFun = true)
        repository = NetworkRepository(mockApi)
    }

    @Test
    fun `get all characters`() {
        every { mockApi.getAllCharacters() } returns CompletableDeferred(
            CharactersResponse(
                listOf(character1, character2),
                Info("2")
            )
        )

        runBlocking { assertEquals(listOf(character1, character2), repository.getAllCharacters()) }

        coVerify { mockApi.getAllCharacters() }

        confirmVerified(mockApi)
    }

    @Test
    fun `get characters`() {
        val expectedResult = 2 to listOf(character1, character2)
        every { mockApi.getNextPageCharacters(1) } returns CompletableDeferred(
            CharactersResponse(
                listOf(character1, character2),
                Info("2")
            )
        )

        runBlocking { assertEquals(expectedResult, repository.getCharacters(1)) }

        coVerify { mockApi.getNextPageCharacters(1) }

        confirmVerified(mockApi)
    }

    @Test
    fun `get characters (next page = -1)`() {
        val expectedResult = -1 to listOf<Character>()

        runBlocking {
            assertEquals(expectedResult, repository.getCharacters(-1))
        }
    }

    @Test
    fun `get episode`() {
        every { mockApi.getEpisode(1) } returns CompletableDeferred(episode1)

        runBlocking { assertEquals(episode1, repository.getEpisode(1)) }

        coVerify { mockApi.getEpisode(1) }

        confirmVerified(mockApi)
    }

    @Test
    fun `get multiple characters`() {
        every { mockApi.getMultipleCharacters("1,2") } returns CompletableDeferred(listOf(character1, character2))

        runBlocking {
            assertEquals(
                listOf(character1, character2),
                repository.getCharacters(listOf("example/1", "example/2"))
            )
        }

        coVerify { mockApi.getMultipleCharacters("1,2") }

        confirmVerified(mockApi)
    }

    @Test
    fun `search characters`() {
        val characters1 = listOf(character1)
        val characters20 = List(20) { character2 }

        every { mockApi.searchCharacter("rick") } returns CompletableDeferred(
            CharactersResponse(characters1, Info(""))
        )

        every { mockApi.searchCharacter("morty") } returns CompletableDeferred(
            CharactersResponse(characters20, Info(""))
        )

        every { mockApi.searchCharacter("Mr.Poopybutthole") } throws Exception()

        runBlocking {
            assertEquals(listOf(character1), repository.searchCharacters("rick"))
            assertEquals(10, repository.searchCharacters("morty").size)
            assertEquals(listOf<Character>(), repository.searchCharacters("Mr.Poopybutthole"))
        }

        coVerify {
            mockApi.searchCharacter("rick")
            mockApi.searchCharacter("morty")
            mockApi.searchCharacter("Mr.Poopybutthole")
        }

        confirmVerified(mockApi)
    }

    @Test
    fun `random characters`() {
        val episodes = listOf(episode1, episode2)
        val expectedResult = listOf(
            Episode(1, "Pilot", "", "", listOf(character1, character2), listOf("1", "2"), false),
            Episode(2, "Pilot2", "", "", listOf(character1, character2), listOf("1", "2"), false)
        )
        every { mockApi.getMultipleCharacters(any()) } returns CompletableDeferred(listOf(character1, character2))

        runBlocking { assertEquals(expectedResult, repository.randomCharacters(episodes)) }

        coVerify {
            mockApi.getMultipleCharacters(or("2,1", "1,2"))
        }

        confirmVerified(mockApi)
    }
}