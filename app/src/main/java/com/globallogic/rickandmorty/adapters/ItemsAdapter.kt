package com.globallogic.rickandmorty.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.extensions.inflate
import com.globallogic.rickandmorty.common.extensions.isFavorite
import com.globallogic.rickandmorty.common.extensions.isSameId
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode

open class ItemsAdapter(private val onFavoriteChanged: (obj: Any, isFavorite: Boolean) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val CHARACTER_TYPE: Int = 0
        private const val EPISODE_TYPE: Int = 1
        private const val EPISODE_TYPE_WITHOUT_CHARACTERS = 2
    }

    var items: List<Any> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun updateItem(item: Any) {
        val index = items
            .indexOfFirst {
                when (it) {
                    is Character -> it.isSameId(item)
                    is Episode -> it.isSameId(item)
                    else -> false
                }
            }

        if (index != -1) {
            items[index].isFavorite = item.isFavorite
            notifyItemChanged(index)
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (val obj = items[position]) {
            is Character -> CHARACTER_TYPE
            is Episode -> if (obj.characters?.isNullOrEmpty() == true) EPISODE_TYPE_WITHOUT_CHARACTERS else EPISODE_TYPE
            else -> error("Not support type")
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            CHARACTER_TYPE -> CharactersViewHolder(parent.inflate(R.layout.layout_character))
            EPISODE_TYPE -> EpisodesViewHolder(parent.inflate(R.layout.layout_episode))
            EPISODE_TYPE_WITHOUT_CHARACTERS -> EpisodesViewHolder(parent.inflate(R.layout.layout_episode_without_characters))
            else -> error("Not supported type")
        }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val obj = items[position]) {
            is Character -> (holder as? CharactersViewHolder)?.bindCharacter(obj, onFavoriteChanged)
            is Episode -> (holder as? EpisodesViewHolder)?.bindEpisode(obj, onFavoriteChanged)
        }
    }
}