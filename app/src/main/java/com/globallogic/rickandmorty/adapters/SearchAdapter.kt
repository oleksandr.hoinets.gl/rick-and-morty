package com.globallogic.rickandmorty.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.extensions.inflate
import com.globallogic.rickandmorty.common.model.SearchHeader
import com.globallogic.rickandmorty.common.model.SearchNoResults
import com.globallogic.rickandmorty.common.model.SearchSeparator
import com.globallogic.rickandmorty.common.model.SwitchToSourceButton
import kotlinx.android.synthetic.main.layout_search_button.view.*
import kotlinx.android.synthetic.main.layout_search_header.view.*

class SearchAdapter(
    onFavoriteChanged: (obj: Any, isFavorite: Boolean) -> Unit,
    private val onOnlineButtonClicked: () -> Unit
) : ItemsAdapter(onFavoriteChanged) {
    companion object {
        private const val TYPE_HEADER = 3
        private const val TYPE_SEPARATOR = 4
        private const val TYPE_BUTTON = 5
        private const val TYPE_NO_RESULTS = 6
    }

    override fun getItemViewType(position: Int) = when (items[position]) {
        is SearchHeader -> TYPE_HEADER
        is SearchSeparator -> TYPE_SEPARATOR
        is SwitchToSourceButton -> TYPE_BUTTON
        is SearchNoResults -> TYPE_NO_RESULTS
        else -> super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        TYPE_HEADER -> SearchHeaderViewHolder(parent.inflate(R.layout.layout_search_header))
        TYPE_SEPARATOR -> ViewHolder(parent.inflate(R.layout.layout_separator))
        TYPE_BUTTON -> SearchButtonViewHolder(parent.inflate(R.layout.layout_search_button))
        TYPE_NO_RESULTS -> ViewHolder(parent.inflate(R.layout.layout_search_no_results))
        else -> super.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val obj = items[position]) {
            is SearchHeader -> (holder as? SearchHeaderViewHolder)?.bindHeader(obj)
            is SwitchToSourceButton -> (holder as? SearchButtonViewHolder)?.bindButton(obj, onOnlineButtonClicked)
            else -> super.onBindViewHolder(holder, position)
        }
    }

    private class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    private class SearchHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindHeader(header: SearchHeader) {
            itemView.tvSearchHeader.setText(header.res)
        }
    }

    private class SearchButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindButton(button: SwitchToSourceButton, action: () -> Unit) {
            itemView.btnSearch.setText(button.res)
            itemView.btnSearch.setOnClickListener {
                action()
            }
        }
    }
}