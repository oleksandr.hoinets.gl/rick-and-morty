package com.globallogic.rickandmorty.adapters

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.datasource.CharactersDiffUtilCallback
import com.globallogic.rickandmorty.extensions.inflate
import com.globallogic.rickandmorty.common.extensions.isSameId
import com.globallogic.rickandmorty.common.model.Character

class CharactersAdapter(private val onFavoriteChanged: (Character, Boolean) -> Unit) :
    PagedListAdapter<Character, CharactersViewHolder>(CharactersDiffUtilCallback()) {

    fun updateCharacter(character: Character) {
        val index = (0 until itemCount).mapNotNull { getItem(it) }.indexOfFirst { it.isSameId(character) }

        if (index != -1) {
            getItem(index)?.isFavorite = character.isFavorite
            notifyItemChanged(index)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharactersViewHolder =
        CharactersViewHolder(parent.inflate(R.layout.layout_character))

    override fun onBindViewHolder(holder: CharactersViewHolder, position: Int) {
        getItem(position)?.let { holder.bindCharacter(it, onFavoriteChanged) }
    }
}