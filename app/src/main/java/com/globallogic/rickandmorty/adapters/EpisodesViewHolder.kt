package com.globallogic.rickandmorty.adapters

import android.view.View
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.activities.CharacterInfoActivity
import com.globallogic.rickandmorty.activities.EpisodeInfoActivity
import com.globallogic.rickandmorty.common.extensions.findAllRegex
import com.globallogic.rickandmorty.extensions.loadImage
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import kotlinx.android.synthetic.main.layout_characters_for_episode.view.*
import kotlinx.android.synthetic.main.layout_episode.view.*

class EpisodesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindEpisode(
        episode: Episode,
        onFavoriteChanged: (episode: Episode, isFavorite: Boolean) -> Unit
    ) {
        itemView.apply {
            tvEpisodeName.text = episode.name
            tvAired.text = episode.dateAired

            val label = episode.label.findAllRegex("\\D\\d\\d")
            tvEpisode.text = label.joinToString(":")

            for (index in 0 until llCharacters.childCount) {
                episode.characters?.getOrNull(index).let { character ->
                    val imageView = (llCharacters.getChildAt(index) as ImageView)
                    imageView.isVisible = character != null
                    character?.let { imageView.setCharacter(it) }
                }
            }

            cbFavoriteEpisode.apply {
                setOnCheckedChangeListener(null)
                isChecked = episode.isFavorite
                setOnCheckedChangeListener { _, isChecked -> onFavoriteChanged(episode, isChecked) }
            }

            setOnClickListener { startEpisodeInfoActivity(episode) }
        }
    }

    private fun startEpisodeInfoActivity(episode: Episode) {
        itemView.context.startActivity(EpisodeInfoActivity.newIntent(itemView.context, episode))
    }

    private fun ImageView.setCharacter(character: Character) {
        loadImage(character.image, true)
        setOnClickListener { context.startActivity(CharacterInfoActivity.newIntent(context, character)) }
    }
}