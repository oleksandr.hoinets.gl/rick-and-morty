package com.globallogic.rickandmorty.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.activities.EpisodeInfoActivity
import com.globallogic.rickandmorty.common.extensions.findAllRegex
import com.globallogic.rickandmorty.extensions.loadImage
import com.globallogic.rickandmorty.common.model.Episode
import kotlinx.android.synthetic.main.layout_episode_mini.view.*

class CharacterInfoEpisodesAdapter(private val episodes: List<Episode>) : RecyclerView.Adapter<CharacterInfoEpisodesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterInfoEpisodesAdapter.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_episode_mini, parent, false))

    override fun getItemCount(): Int = episodes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindMiniEpisode(episodes[position])

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindMiniEpisode(episode: Episode) {
            itemView.apply {
                val label = episode.label.findAllRegex("\\D\\d\\d")
                tvEpisodeLabel.text = label.joinToString(":")
                imgEpisode.loadImage("http://cdn.madman.com.au/images/series/18601-series-header.jpg")

                setOnClickListener { context.startActivity(EpisodeInfoActivity.newIntent(context, episode)) }
            }
        }
    }
}
