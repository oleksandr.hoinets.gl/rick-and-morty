package com.globallogic.rickandmorty.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.activities.CharacterInfoActivity
import com.globallogic.rickandmorty.extensions.loadImage
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Status
import kotlinx.android.synthetic.main.layout_character.view.*

class CharactersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindCharacter(
        character: Character,
        onFavoriteChanged: (character: Character, isFavorite: Boolean) -> Unit
    ) {
        itemView.apply {
            imgAvatar.loadImage(character.image, true)
            tvName.text = character.name

            val (colorRes, statusRes) = when (character.status ?: Status.UNKNOWN) {
                Status.ALIVE -> R.color.status_alive to R.string.status_alive
                Status.DEAD -> R.color.status_dead to R.string.status_dead
                Status.UNKNOWN -> R.color.status_unknown to R.string.status_unknown
            }

            tvStatus.text = context.resources.getText(statusRes)
            tvStatus.setTextColor(context.resources.getColor(colorRes))

            cbFavoriteCharacter.apply {
                setOnCheckedChangeListener(null)
                isChecked = character.isFavorite
                setOnCheckedChangeListener { _, isChecked -> onFavoriteChanged(character, isChecked) }
            }

            setOnClickListener { startCharacterInfoActivity(character) }
        }
    }

    private fun startCharacterInfoActivity(character: Character) {
        itemView.context.startActivity(CharacterInfoActivity.newIntent(itemView.context, character))
    }
}