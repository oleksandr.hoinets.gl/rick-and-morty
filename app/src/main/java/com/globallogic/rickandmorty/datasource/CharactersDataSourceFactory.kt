package com.globallogic.rickandmorty.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.globallogic.rickandmorty.common.model.Character

class CharactersDataSourceFactory : DataSource.Factory<Int, Character>() {
    val dataSource = MutableLiveData<CharactersDataSource>()

    override fun create(): DataSource<Int, Character> = CharactersDataSource().apply { dataSource.postValue(this) }
}