package com.globallogic.rickandmorty.datasource

import androidx.recyclerview.widget.DiffUtil
import com.globallogic.rickandmorty.common.model.Character

class CharactersDiffUtilCallback : DiffUtil.ItemCallback<Character>() {
    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean = oldItem == newItem
}