package com.globallogic.rickandmorty.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class CharactersDataSource : PageKeyedDataSource<Int, Character>(), CoroutineScope, KoinComponent {
    override val coroutineContext = Dispatchers.IO

    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable?>()
    val errorLoadingNext = MutableLiveData<Throwable?>()

    private val repository by inject<Repository>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Character>) {
        launch {
            try {
                loading.postValue(true)
                val response = repository.getCharacters()
                val nextPage = response.first
                val data = response.second

                callback.onResult(data, null, nextPage)
                error.postValue(null)
            } catch (e: Exception) {
                error.postValue(e)
            } finally {
                loading.postValue(false)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
        launch {
            try {
                val response = repository.getCharacters(params.key)

                val (nextPage, data) = if (response.first == -1) {
                    null to listOf<Character>()
                } else {
                    response.first to response.second
                }

                callback.onResult(data, nextPage)
                errorLoadingNext.postValue(null)
            } catch (e: Exception) {
                callback.onResult(listOf(), null)
                errorLoadingNext.postValue(e)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) = Unit
}