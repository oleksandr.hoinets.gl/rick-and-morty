package com.globallogic.rickandmorty.activities

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Rect
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.CharacterInfoEpisodesAdapter
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Status
import com.globallogic.rickandmorty.extensions.loadImage
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.EpisodeViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.activity_character_info.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharacterInfoActivity : AppCompatActivity() {
    companion object {
        private const val KEY_CHARACTER = "key.character"

        fun newIntent(context: Context, character: Character): Intent =
            Intent(context, CharacterInfoActivity::class.java).apply { putExtra(KEY_CHARACTER, character) }
    }

    private val episodeViewModel by viewModel<EpisodeViewModel>()
    private val favoriteViewModel by viewModel<FavoriteViewModel>()
    private val downloadedViewModel by viewModel<DownloadedViewModel>()

    private val character: Character by lazy { intent.getParcelableExtra<Character>(KEY_CHARACTER) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_info)
        setSupportActionBar(tbCharacterInfo)
        supportActionBar?.title = null

        character.apply {
            imgAvatar.loadImage(image, true)
            tvCharacterNameToolbar.text = name
            tvSpecies.text = species
            tvGender.text = gender
            tvStatus.setText(
                when (status ?: Status.UNKNOWN) {
                    Status.ALIVE -> R.string.status_alive
                    Status.DEAD -> R.string.status_dead
                    Status.UNKNOWN -> R.string.status_unknown
                }
            )
            tvLocation.text = getString(R.string.location, location.name)
            rvEpisodes.apply {
                addItemDecoration(object : RecyclerView.ItemDecoration() {
                    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
                        val isLandscape = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
                        val smallOffset = resources.getDimensionPixelSize(R.dimen.margin_tiny)
                        val offset = resources.getDimensionPixelSize(R.dimen.margin_small)
                        val startOffset = if (isLandscape) smallOffset else if (itemPosition == 0) offset else 0
                        val endOffset = if (isLandscape) 0 else offset

                        outRect.set(startOffset, smallOffset, endOffset, smallOffset)
                    }
                })
            }
        }

        cbFavoriteCharacterInfo.setOnCheckedChangeListener { _, isChecked ->
            character.isFavorite = isChecked
            favoriteViewModel.setFavorite(character, isChecked)
        }

        cbDownloadCharacterInfo.setOnClickListener {
            downloadedViewModel.download(character, cbDownloadCharacterInfo.isChecked)
        }

        observe(downloadedViewModel.isDownloaded) { cbDownloadCharacterInfo.isChecked = it }
        observe(favoriteViewModel.character) { cbFavoriteCharacterInfo.isChecked = it.isFavorite }

        episodeViewModel.apply {
            observe(episodes) {
                tvEpisodes.isVisible = true
                rvEpisodes.apply {
                    layoutManager = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                        GridLayoutManager(context, 3) else LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    adapter = CharacterInfoEpisodesAdapter(it)
                }
                evCharacterInfo.hideError()
            }

            observe(isLoading) { if (it) evCharacterInfo.showLoading() else evCharacterInfo.hideLoading() }
            observe(error) { evCharacterInfo.setError(it) }

            loadEpisodes(character)
            evCharacterInfo.onRetryClicked = { reloadEpisodes(character) }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onStart() {
        super.onStart()
        favoriteViewModel.fetchCharacterFavoriteStatus(character)
        downloadedViewModel.fetchDownloadedStatus(character)
    }
}