package com.globallogic.rickandmorty.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.SearchAdapter
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.extensions.setOnTextChangedListener
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import com.globallogic.rickandmorty.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity() {
    private val searchViewModel by viewModel<SearchViewModel>()
    private val favoriteViewModel by viewModel<FavoriteViewModel>()

    private val adapter = SearchAdapter({ obj, isFavorite ->
        when (obj) {
            is Character -> favoriteViewModel.setFavorite(obj, isFavorite)
            is Episode -> favoriteViewModel.setFavorite(obj, isFavorite)
        }
    }, { searchViewModel.toggleOnline() })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(tbSearch)

        rvSearchResults.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvSearchResults.adapter = adapter

        etSearch.setOnTextChangedListener { searchViewModel.search(it) }

        searchViewModel.apply {
            observe(searchResults) { adapter.items = it }
            observe(isLoading) {
                pbSearch.isVisible = it
                imgSearchItems.isVisible = !it
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}