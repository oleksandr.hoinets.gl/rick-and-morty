package com.globallogic.rickandmorty.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.fragments.CharactersListFragment
import com.globallogic.rickandmorty.fragments.DownloadsFragment
import com.globallogic.rickandmorty.fragments.EpisodesListFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private var fragments: Map<Int, Fragment> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(tbHome)

        populateFragment(R.id.itemCharactersList) { CharactersListFragment() }
        populateFragment(R.id.itemEpisodeList) { EpisodesListFragment() }
        populateFragment(R.id.itemDownloads) { DownloadsFragment() }

        bottomNavigationBar.apply {
            setOnNavigationItemSelectedListener {
                showFragment(it.itemId)
                true
            }
            selectedItemId = R.id.itemCharactersList
            setOnNavigationItemReselectedListener {}
        }

        imgSearch.setOnClickListener { startActivity(Intent(this, SearchActivity::class.java)) }
    }

    private fun <T : Fragment> populateFragment(id: Int, initBlock: () -> T) {
        val fragment = supportFragmentManager.findFragmentByTag("$id") ?: run {
            val newFragment = initBlock()
            supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, newFragment, "$id").commit()
            newFragment
        }
        fragments += id to fragment
    }

    private fun showFragment(itemId: Int) {
        supportFragmentManager.beginTransaction().apply {
            fragments.values.forEach { hide(it) }
            fragments[itemId]?.let { show(it) }
            commit()
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        showFragment(bottomNavigationBar.selectedItemId)
    }
}
