package com.globallogic.rickandmorty.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.ItemsAdapter
import com.globallogic.rickandmorty.common.extensions.findAllRegex
import com.globallogic.rickandmorty.extensions.loadImage
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.viewmodel.CharacterViewModel
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.activity_episode_info.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EpisodeInfoActivity : AppCompatActivity() {
    companion object {
        private const val KEY_EPISODE = "key.episode"
        fun newIntent(context: Context, episode: Episode): Intent =
            Intent(context, EpisodeInfoActivity::class.java).apply {
                putExtra(KEY_EPISODE, episode)
            }
    }

    private val characterViewModel by viewModel<CharacterViewModel>()
    private val favoriteViewModel by viewModel<FavoriteViewModel>()
    private val downloadedViewModel by viewModel<DownloadedViewModel>()

    private val adapter = ItemsAdapter { item, isFavorite -> favoriteViewModel.setFavorite(item, isFavorite) }

    private val episode: Episode by lazy { intent.getParcelableExtra<Episode>(KEY_EPISODE) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_episode_info)

        rvCharacters.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvCharacters.adapter = adapter

        episode.apply {
            imgEpisodeAvatar.loadImage("http://cdn.madman.com.au/images/series/18601-series-header.jpg")
            val labelEpisode = label.findAllRegex("\\D\\d\\d")
            tvEpisodeLabelInfo.text = labelEpisode.joinToString(":")
            tvEpisodeNameInfoToolbar.text = name
            tvDate.text = dateAired
        }

        cbFavoriteEpisodeInfo.setOnCheckedChangeListener { _, isChecked ->
            episode.isFavorite = isChecked
            favoriteViewModel.setFavorite(episode, isChecked)
        }

        cbDownloadEpisodeInfo.setOnClickListener {
            downloadedViewModel.download(episode, cbDownloadEpisodeInfo.isChecked)
        }

        observe(downloadedViewModel.isDownloaded) { cbDownloadEpisodeInfo.isChecked = it }
        observe(favoriteViewModel.episode) { cbFavoriteEpisodeInfo.isChecked = it.isFavorite }

        characterViewModel.apply {
            evEpisodeInfo.onRetryClicked = { reloadCharacters(episode) }

            observe(characters) {
                favoriteViewModel.fetchCharacterFavoriteStatus(it)
                adapter.items = it
                evEpisodeInfo.hideError()

            }
            observe(isLoading) { if (it) evEpisodeInfo.showLoading() else evEpisodeInfo.hideLoading() }
            observe(error) { evEpisodeInfo.setError(it) }

            loadCharacters(episode)
        }

        observe(favoriteViewModel.characters) { adapter.items = it }

        setSupportActionBar(tbEpisodeInfo)
        supportActionBar?.title = null
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onStart() {
        super.onStart()
        favoriteViewModel.fetchEpisodeFavoriteStatus(episode)
        favoriteViewModel.reloadCharacterFavoriteStatus()
        downloadedViewModel.fetchDownloadedStatus(episode)
    }
}
