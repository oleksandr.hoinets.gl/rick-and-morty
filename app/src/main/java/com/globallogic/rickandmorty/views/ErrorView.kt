package com.globallogic.rickandmorty.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.globallogic.rickandmorty.R
import kotlinx.android.synthetic.main.layout_error_message.view.*
import retrofit2.HttpException
import java.net.UnknownHostException

class ErrorView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr) {
    var onRetryClicked: (() -> Unit)? = null
        set(value) = btnErrorRetry.setOnClickListener { value?.invoke() }

    private var shouldShowError = false

    var error: String?
        set(value) {
            tvErrorMessage.text = value
            shouldShowError = true
            showError()
        }
        get() = tvErrorMessage.text.toString()

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_error_message, this, true)
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.ErrorView, 0, 0)
        error = ta.getString(R.styleable.ErrorView_errorString)
        hideError()
        ta.recycle()
    }

    fun setLoading(srl: SwipeRefreshLayout, isLoading: Boolean) {
        if (isLoading) {
            if (!srl.isRefreshing) showLoading()
        } else {
            srl.isRefreshing = false
            hideLoading()
        }
    }

    fun setError(exception: Throwable) {
        error = exception.getErrorMessage(context)
    }

    fun showLoading() { //todo remove
        isVisible = true
        pbErrorLoading.isVisible = true
        tvErrorMessage.isVisible = shouldShowError
        btnErrorRetry.isVisible = shouldShowError
    }

    fun hideLoading() { //todo remove
        pbErrorLoading.isVisible = false
    }

    fun showError() {
        shouldShowError = true
        isVisible = true
        pbErrorLoading.isVisible = false
        tvErrorMessage.isVisible = true
        btnErrorRetry.isVisible = true
    }

    fun hideError() {
        shouldShowError = false
        tvErrorMessage.isVisible = false
        btnErrorRetry.isVisible = false
    }
}

fun Throwable.getErrorMessage(context: Context): String =
    context.getString(
        when (this) {
            is UnknownHostException -> R.string.error_no_internet_connection
            is HttpException -> when (code()) {
                in 400..499 -> R.string.error_status_code_400
                in 500..599 -> R.string.error_status_code_500
                else -> R.string.error_something_went_wrong
            }
            else -> R.string.error_something_went_wrong
        }
    )