package com.globallogic.rickandmorty

import android.util.Log
import androidx.room.Room
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.repository.RickAndMortyRepository
import com.globallogic.rickandmorty.repository.database.AppDatabase
import com.globallogic.rickandmorty.repository.database.DatabaseRepository
import com.globallogic.rickandmorty.repository.network.NetworkRepository
import com.globallogic.rickandmorty.repository.network.RickAndMortyApi
import com.globallogic.rickandmorty.viewmodel.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val applicationModule = module {
    single { DatabaseRepository(Room.databaseBuilder(androidContext(), AppDatabase::class.java, "Rick-and-Morty").build()) }

    single { NetworkRepository(get()) }

    single { RickAndMortyRepository(androidContext()) } bind Repository::class
}

val viewModelModule = module {
    viewModel { SearchViewModel(get(), Dispatchers.IO) }

    viewModel { EpisodeViewModel(get(), Dispatchers.IO) }

    viewModel { FavoriteViewModel(get(), Dispatchers.IO) }

    viewModel { CharacterViewModel(get(), Dispatchers.IO) }

    viewModel { DownloadedViewModel(get(), Dispatchers.IO) }
}

val networkModule = module {
    single {
        OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .addInterceptor {
                if (BuildConfig.DEBUG) Log.d("NetworkService", it.request().url().toString())
                it.proceed(it.request())
            }
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com")
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(RickAndMortyApi::class.java)
    }
}