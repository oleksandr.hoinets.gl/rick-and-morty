package com.globallogic.rickandmorty.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.ItemsAdapter
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.extensions.setInfiniteScrollListener
import com.globallogic.rickandmorty.viewmodel.EpisodeViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_episodes_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class EpisodesListFragment : Fragment() {
    private val episodeViewModel by viewModel<EpisodeViewModel>()
    private val favoriteViewModel by sharedViewModel<FavoriteViewModel>()
    private val adapter = ItemsAdapter { item, isFavorite -> favoriteViewModel.setFavorite(item, isFavorite) }

    private var nextPage: Int = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_episodes_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvEpisodesList.itemAnimator = null
        rvEpisodesList.layoutManager = LinearLayoutManager(context)
        rvEpisodesList.adapter = adapter

        episodeViewModel.apply {
            srlEpisodesList.setOnRefreshListener { loadEpisodes() }
            evEpisodesList.onRetryClicked = { loadEpisodes() }

            observe(isLoading) { evEpisodesList.setLoading(srlEpisodesList, it) }
            observe(nextPage) { this@EpisodesListFragment.nextPage = it }
            observe(episodes) {
                favoriteViewModel.fetchEpisodesFavoriteStatus(it)
                rvEpisodesList.isVisible = true
                adapter.items = it
                evEpisodesList.hideError()
            }
            observe(error) {
                rvEpisodesList.isVisible = false
                evEpisodesList.setError(it)
            }

            loadAllEpisodes()
        }

        observe(favoriteViewModel.episodes) { adapter.items = it }
        observe(favoriteViewModel.episode) { adapter.updateItem(it) }

        rvEpisodesList.setInfiniteScrollListener { episodeViewModel.loadEpisodes(nextPage) }
    }

    override fun onStart() {
        super.onStart()
        favoriteViewModel.reloadEpisodeFavoriteStatus()
    }
}