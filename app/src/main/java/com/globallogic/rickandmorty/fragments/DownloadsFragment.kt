package com.globallogic.rickandmorty.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.ItemsAdapter
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.viewmodel.DownloadedViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_downloads_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DownloadsFragment : Fragment() {
    private val favoriteViewModel by sharedViewModel<FavoriteViewModel>()
    private val downloadedViewModel by viewModel<DownloadedViewModel>()

    private val adapter = ItemsAdapter { obj, isFavorite ->
        when (obj) {
            is Character -> favoriteViewModel.setFavorite(obj, isFavorite)
            is Episode -> favoriteViewModel.setFavorite(obj, isFavorite)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_downloads_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvDownloads.itemAnimator = null
        rvDownloads.layoutManager = LinearLayoutManager(context)
        rvDownloads.adapter = adapter

        observe(downloadedViewModel.downloads) { adapter.items = it }
        observe(favoriteViewModel.character) { adapter.updateItem(it) }
        observe(favoriteViewModel.episode) { adapter.updateItem(it) }

        downloadedViewModel.fetchAll()
    }

    override fun onStart() {
        super.onStart()
        downloadedViewModel.fetchAll()
    }
}