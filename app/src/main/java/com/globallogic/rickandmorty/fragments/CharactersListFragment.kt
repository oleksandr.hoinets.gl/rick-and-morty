package com.globallogic.rickandmorty.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.adapters.CharactersAdapter
import com.globallogic.rickandmorty.common.extensions.observe
import com.globallogic.rickandmorty.viewmodel.CharacterViewModel
import com.globallogic.rickandmorty.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_characters_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharactersListFragment : Fragment() {
    private val characterViewModel by viewModel<CharacterViewModel>()
    private val favoriteViewModel by sharedViewModel<FavoriteViewModel>()
    private lateinit var adapter: CharactersAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_characters_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = CharactersAdapter { character, isFavorite ->
            favoriteViewModel.setFavorite(
                character,
                isFavorite
            )
        }

        rvCharactersList.itemAnimator = null
        rvCharactersList.layoutManager = LinearLayoutManager(context)
        rvCharactersList.adapter = adapter

        characterViewModel.apply {
            srlCharactersList.setOnRefreshListener { adapter.currentList?.dataSource?.invalidate() }
            evCharactersList.onRetryClicked = { adapter.currentList?.dataSource?.invalidate() }

            observe(pagedList) { adapter.submitList(it) }
            observe(loading) { evCharactersList.setLoading(srlCharactersList, it) }
            observe(errorLoading) {
                it?.let { error ->
                    rvCharactersList.isVisible = false
                    evCharactersList.setError(error)
                } ?: run {
                    rvCharactersList.isVisible = true
                    evCharactersList.hideError()
                }
            }
            observe(errorLoadingNext) {
                it?.let {
                    Toast.makeText(context, R.string.error_can_not_load_more_characters, Toast.LENGTH_SHORT).show()
                }
            }
        }

        favoriteViewModel.apply {
            observe(character) { adapter.updateCharacter(it) }
            observe(characters) { it.forEach { character -> adapter.updateCharacter(character) } }
        }
    }

    override fun onStart() {
        super.onStart()
        adapter.currentList?.snapshot()?.let { favoriteViewModel.fetchCharacterFavoriteStatus(it) }
    }
}