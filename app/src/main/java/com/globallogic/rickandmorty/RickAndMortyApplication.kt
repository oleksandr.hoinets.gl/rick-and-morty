package com.globallogic.rickandmorty

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import org.koin.log.EmptyLogger

class RickAndMortyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(
            this,
            listOf(applicationModule, viewModelModule, networkModule),
            logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
        )
    }
}