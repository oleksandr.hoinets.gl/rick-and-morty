package com.globallogic.rickandmorty.repository.database

import androidx.room.TypeConverter
import com.globallogic.rickandmorty.common.extensions.findRegex
import com.globallogic.rickandmorty.common.model.Status

class ListConverter {
    @TypeConverter
    fun fromList(data: List<String>) = data.mapNotNull { it.findRegex("\\d+") }.joinToString(",")

    @TypeConverter
    fun toList(data: String) = data.split(",")
}

class EnumConverter {
    @TypeConverter
    fun fromEnum(data: Status?) = data?.name

    @TypeConverter
    fun toEnum(data: String?) = data?.let { Status.valueOf(it) }
}