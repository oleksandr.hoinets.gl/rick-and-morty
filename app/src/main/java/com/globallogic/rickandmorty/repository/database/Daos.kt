package com.globallogic.rickandmorty.repository.database

import androidx.room.*

@Dao
interface CharactersDao {
    @Query("SELECT * FROM characters")
    fun getAllCharacters(): List<CharacterEntity>

    @Query("SELECT * FROM characters WHERE id = :id")
    fun getById(id: Int): CharacterEntity?

    @Query("SELECT * FROM characters WHERE name LIKE :name")
    fun findByName(name: String): List<CharacterEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg characters: CharacterEntity)

    @Delete
    fun delete(character: CharacterEntity)
}

@Dao
interface EpisodesDao {
    @Query("SELECT * FROM episodes")
    fun getAllEpisodes(): List<EpisodeEntity>

    @Query("SELECT * FROM episodes WHERE id = :id")
    fun getById(id: Int): EpisodeEntity?

    @Query("SELECT * FROM episodes WHERE name LIKE :name")
    fun findByName(name: String): List<EpisodeEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg episodes: EpisodeEntity)

    @Delete
    fun delete(episode: EpisodeEntity)
}

@Dao
interface FavoriteCharactersDao {
    @Query("SELECT * FROM favorite_characters")
    fun getAllCharacters(): List<FavoriteCharactersEntity>

    @Query("SELECT * FROM favorite_characters WHERE character_id = :characterId")
    fun getById(characterId: Int): FavoriteCharactersEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg characters: FavoriteCharactersEntity)

    @Delete
    fun delete(character: FavoriteCharactersEntity)
}

@Dao
interface FavoriteEpisodesDao {
    @Query("SELECT * FROM favorite_episodes")
    fun getAllEpisodes(): List<FavoriteEpisodesEntity>

    @Query("SELECT * FROM favorite_episodes WHERE episode_id = :episodeId")
    fun getById(episodeId: Int): FavoriteEpisodesEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg episodes: FavoriteEpisodesEntity)

    @Delete
    fun delete(episode: FavoriteEpisodesEntity)
}