package com.globallogic.rickandmorty.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [EpisodeEntity::class, CharacterEntity::class, FavoriteCharactersEntity::class, FavoriteEpisodesEntity::class],
    version = 1
)
@TypeConverters(ListConverter::class, EnumConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun charactersDao(): CharactersDao
    abstract fun episodesDao(): EpisodesDao
    abstract fun favoriteCharactersDao(): FavoriteCharactersDao
    abstract fun favoriteEpisodesDao(): FavoriteEpisodesDao
}


