package com.globallogic.rickandmorty.repository.network

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.CharactersResponse
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.EpisodesResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickAndMortyApi {
    @GET("/api/character/")
    fun getAllCharacters(): Deferred<CharactersResponse>

    @GET("/api/character/")
    fun getNextPageCharacters(@Query("page") page: Int): Deferred<CharactersResponse>

    @GET("/api/episode/")
    fun getAllEpisodes(): Deferred<EpisodesResponse>

    @GET("/api/episode/")
    fun getNextPageEpisodes(@Query("page") page: Int): Deferred<EpisodesResponse>

    @GET("/api/character/{ids}")
    fun getMultipleCharacters(@Path("ids") ids: String): Deferred<List<Character>>

    @GET("/api/character/{id}")
    fun getCharacter(@Path("id") id: Int): Deferred<Character>

    @GET("/api/episode/{id}")
    fun getEpisode(@Path("id") id: Int): Deferred<Episode>

    @GET("/api/episode/{ids}")
    fun getMultipleEpisodes(@Path("ids") ids: String): Deferred<List<Episode>>

    @GET("/api/character/")
    fun searchCharacter(@Query("name") name: String): Deferred<CharactersResponse>

    @GET("/api/episode/")
    fun searchEpisode(@Query("name") name: String): Deferred<EpisodesResponse>
}