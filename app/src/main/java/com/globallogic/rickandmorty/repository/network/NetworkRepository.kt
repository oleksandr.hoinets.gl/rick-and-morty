package com.globallogic.rickandmorty.repository.network

import com.globallogic.rickandmorty.common.extensions.findRegex
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Favorite
import com.globallogic.rickandmorty.repository.Repository

class NetworkRepository(private val api: RickAndMortyApi) : Repository {

    override suspend fun getAllCharacters(offlineOnly: Boolean): List<Character> =
        api.getAllCharacters().await().results

    override suspend fun getAllEpisodes(offlineOnly: Boolean): List<Episode> =
        api.getAllEpisodes().await().results

    override suspend fun getCharacters(page: Int): Pair<Int, List<Character>> {
        if (page == -1) return page to listOf()

        val response = api.getNextPageCharacters(page).await()
        val nextPage = response.info.next.findRegex("\\d+")?.toIntOrNull() ?: -1
        val data = response.results

        return nextPage to data
    }

    override suspend fun getEpisodes(page: Int): Pair<Int, List<Episode>> {
        if (page == -1) return page to listOf()

        val response = api.getNextPageEpisodes(page).await()
        val nextPage = response.info.next.findRegex("\\d+")?.toIntOrNull() ?: -1
        val data = response.results

        return nextPage to data
    }

    override suspend fun getEpisode(id: Int): Episode = api.getEpisode(id).await()

    override suspend fun getCharacters(ids: List<String>): List<Character> {
        val charactersIds = ids.mapNotNull { it.findRegex("\\d+") }.joinToString(",")
        return if (ids.size == 1) {
            listOf(api.getCharacter(charactersIds.toInt()).await())
        } else {
            api.getMultipleCharacters(charactersIds).await()
        }
    }

    override suspend fun getEpisodes(ids: List<String>): List<Episode> {
        val episodesIds = ids.mapNotNull { it.findRegex("\\d+") }.joinToString(",")
        return api.getMultipleEpisodes(episodesIds).await()
    }

    override suspend fun searchCharacters(query: String, offlineOnly: Boolean): List<Character> =
        try {
            api.searchCharacter(query).await().results.take(10)
        } catch (e: Exception) {
            listOf()
        }

    override suspend fun searchEpisodes(query: String, offlineOnly: Boolean): List<Episode> =
        try {
            randomCharacters(api.searchEpisode(query).await().results.take(10))
        } catch (e: Exception) {
            listOf()
        }

    override suspend fun randomCharacters(episodes: List<Episode>): List<Episode> = episodes
        .map {
            val charactersIds = getRandomCharacters(it)
            it to api.getMultipleCharacters(charactersIds)
        }
        .map { it.first.apply { characters = it.second.await() } }

    private fun getRandomCharacters(episode: Episode) =
        episode.characterUrls
            .shuffled()
            .take(8)
            .mapNotNull { it.findRegex("\\d+") }
            .joinToString(",")

    override suspend fun insertToDownload(item: Any) {}
    override suspend fun isFavorite(favorite: Favorite): Boolean = false
    override suspend fun deleteFromDownload(item: Any) {}
    override suspend fun updateFavoriteStatus(item: Any) {}
    override suspend fun isDownloaded(item: Any): Boolean = false
}
