package com.globallogic.rickandmorty.repository

import android.content.Context
import com.globallogic.rickandmorty.common.extensions.isOnline
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Favorite
import com.globallogic.rickandmorty.repository.database.DatabaseRepository
import com.globallogic.rickandmorty.repository.network.NetworkRepository
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.net.UnknownHostException

class RickAndMortyRepository(private val context: Context) : Repository, KoinComponent {
    private val networkRepository by inject<NetworkRepository>()
    private val databaseRepository by inject<DatabaseRepository>()

    override suspend fun getAllCharacters(offlineOnly: Boolean): List<Character> =
        execute(
            { networkRepository.getAllCharacters() },
            { databaseRepository.getAllCharacters() },
            offlineOnly
        ).updateFavorites()

    override suspend fun getAllEpisodes(offlineOnly: Boolean): List<Episode> =
        execute(
            { networkRepository.getAllEpisodes() },
            { databaseRepository.getAllEpisodes() },
            offlineOnly
        ).updateFavorites()

    override suspend fun getCharacters(page: Int): Pair<Int, List<Character>> =
        execute(
            { networkRepository.getCharacters(page) },
            { databaseRepository.getCharacters(page) }
        ).updateFavorites()

    override suspend fun getEpisodes(page: Int): Pair<Int, List<Episode>> =
        execute(
            { networkRepository.getEpisodes(page) },
            { databaseRepository.getEpisodes(page) }
        ).updateFavorites()

    override suspend fun searchCharacters(query: String, offlineOnly: Boolean): List<Character> =
        execute(
            { networkRepository.searchCharacters(query) },
            { databaseRepository.searchCharacters(query) },
            offlineOnly
        ).updateFavorites()

    override suspend fun searchEpisodes(query: String, offlineOnly: Boolean): List<Episode> =
        execute(
            { networkRepository.searchEpisodes(query) },
            { databaseRepository.searchEpisodes(query) },
            offlineOnly
        ).updateFavorites()

    override suspend fun getCharacters(ids: List<String>): List<Character> =
        execute(
            { networkRepository.getCharacters(ids) },
            { databaseRepository.getCharacters(ids) }
        ).updateFavorites()

    override suspend fun getEpisodes(ids: List<String>): List<Episode> =
        execute(
            { networkRepository.getEpisodes(ids) },
            { databaseRepository.getEpisodes(ids) }
        ).updateFavorites()

    override suspend fun randomCharacters(episodes: List<Episode>): List<Episode> =
        execute(
            { networkRepository.randomCharacters(episodes) },
            { databaseRepository.randomCharacters(episodes) }
        ).updateFavorites()

    override suspend fun getEpisode(id: Int): Episode =
        execute(
            { networkRepository.getEpisode(id) },
            { databaseRepository.getEpisode(id) }
        ).apply { this.isFavorite = isFavorite(this) }

    override suspend fun isFavorite(favorite: Favorite): Boolean = databaseRepository.isFavorite(favorite)

    override suspend fun insertToDownload(item: Any) = databaseRepository.insertToDownload(item)

    override suspend fun deleteFromDownload(item: Any) = databaseRepository.deleteFromDownload(item)

    override suspend fun updateFavoriteStatus(item: Any) = databaseRepository.updateFavoriteStatus(item)

    override suspend fun isDownloaded(item: Any): Boolean = databaseRepository.isDownloaded(item)

    private suspend fun <T : Favorite> List<T>.updateFavorites() = onEach { it.isFavorite = isFavorite(it) }
    private suspend fun <T : Favorite> Pair<Int, List<T>>.updateFavorites() = apply { second.updateFavorites() }

    private suspend fun <T> execute(
        online: suspend () -> T,
        offline: suspend () -> T,
        offlineOnly: Boolean = false
    ): T {
        return if (context.isOnline() && !offlineOnly) {
            try {
                online()
            } catch (e: UnknownHostException) {
                offline()
            }
        } else {
            offline()
        }
    }
}