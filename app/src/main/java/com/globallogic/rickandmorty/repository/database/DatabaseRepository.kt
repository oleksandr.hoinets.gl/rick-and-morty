package com.globallogic.rickandmorty.repository.database

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Favorite
import com.globallogic.rickandmorty.repository.Repository

class DatabaseRepository(private val database: AppDatabase) : Repository {

    override suspend fun isFavorite(favorite: Favorite): Boolean =
        when (favorite) {
            is Episode -> database.favoriteEpisodesDao().getById(favorite.id)?.isFavorite
            is Character -> database.favoriteCharactersDao().getById(favorite.id)?.isFavorite
            else -> error("WTF it can't be here")
        } ?: false

    override suspend fun insertToDownload(item: Any) {
        when (item) {
            is Character -> database.charactersDao().insert(item.toEntity())
            is Episode -> database.episodesDao().insert(item.toEntity())
            else -> error("WTF it can't be here")
        }
    }

    override suspend fun deleteFromDownload(item: Any) {
        when (item) {
            is Character -> database.charactersDao().delete(item.toEntity())
            is Episode -> database.episodesDao().delete(item.toEntity())
            else -> error("WTF it can't be here")
        }
    }

    override suspend fun updateFavoriteStatus(item: Any) {
        when (item) {
            is Character -> {
                val character = database.favoriteCharactersDao().getById(item.id)
                database.favoriteCharactersDao()
                    .insert(FavoriteCharactersEntity(character?.id ?: 0, item.id, item.isFavorite))
            }
            is Episode -> {
                val episode = database.favoriteEpisodesDao().getById(item.id)
                database.favoriteEpisodesDao()
                    .insert(FavoriteEpisodesEntity(episode?.id ?: 0, item.id, item.isFavorite))
            }
            else -> error("WTF it can't be here")
        }
    }

    override suspend fun isDownloaded(item: Any): Boolean =
        when (item) {
            is Character -> database.charactersDao().getById(item.id) != null
            is Episode -> database.episodesDao().getById(item.id) != null
            else -> error("WTF it can't be here")
        }

    override suspend fun getAllCharacters(offlineOnly: Boolean): List<Character> =
        database.charactersDao().getAllCharacters().map { it.toCharacter() }

    override suspend fun getAllEpisodes(offlineOnly: Boolean): List<Episode> =
        randomCharacters(database.episodesDao().getAllEpisodes().map { it.toEpisode() })

    override suspend fun searchCharacters(query: String, offlineOnly: Boolean): List<Character> =
        database.charactersDao().findByName("%$query%").map { it.toCharacter() }.take(10)

    override suspend fun searchEpisodes(query: String, offlineOnly: Boolean): List<Episode> =
        randomCharacters(database.episodesDao().findByName("%$query%").map { it.toEpisode() }.take(10))

    override suspend fun getCharacters(ids: List<String>): List<Character> =
        ids.mapNotNull { database.charactersDao().getById(it.toInt())?.toCharacter() }

    override suspend fun getEpisodes(ids: List<String>): List<Episode> =
        ids.mapNotNull { database.episodesDao().getById(it.toInt())?.toEpisode() }

    override suspend fun randomCharacters(episodes: List<Episode>): List<Episode> =
        episodes.onEach {
            it.characters =
                it.characterUrls.mapNotNull { id -> database.charactersDao().getById(id.toInt())?.toCharacter() }
        }

    override suspend fun getEpisode(id: Int): Episode =
        database.episodesDao().getById(id)?.toEpisode() ?: error("wtf")

    override suspend fun getCharacters(page: Int): Pair<Int, List<Character>> =
        -1 to database.charactersDao().getAllCharacters().map { it.toCharacter() }

    override suspend fun getEpisodes(page: Int): Pair<Int, List<Episode>> =
        -1 to database.episodesDao().getAllEpisodes().map { it.toEpisode() }
}