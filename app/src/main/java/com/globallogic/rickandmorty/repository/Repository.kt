package com.globallogic.rickandmorty.repository

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Favorite

interface Repository {
    suspend fun getAllEpisodes(offlineOnly: Boolean = false): List<Episode>
    suspend fun getAllCharacters(offlineOnly: Boolean = false): List<Character>
    suspend fun getEpisodes(ids: List<String>): List<Episode>
    suspend fun getCharacters(ids: List<String>): List<Character>
    suspend fun getEpisode(id: Int): Episode
    suspend fun getCharacters(page: Int = 1): Pair<Int, List<Character>>
    suspend fun getEpisodes(page: Int = 1): Pair<Int, List<Episode>>
    suspend fun randomCharacters(episodes: List<Episode>): List<Episode>
    suspend fun searchCharacters(query: String, offlineOnly: Boolean = false): List<Character>
    suspend fun searchEpisodes(query: String, offlineOnly: Boolean = false): List<Episode>

    suspend fun insertToDownload(item: Any)
    suspend fun deleteFromDownload(item: Any)
    suspend fun isDownloaded(item: Any): Boolean
    suspend fun isFavorite(favorite: Favorite): Boolean
    suspend fun updateFavoriteStatus(item: Any)
}

