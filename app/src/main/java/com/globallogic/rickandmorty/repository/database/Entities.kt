package com.globallogic.rickandmorty.repository.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.common.model.Location
import com.globallogic.rickandmorty.common.model.Status

@Entity(tableName = "episodes")
data class EpisodeEntity(
    @PrimaryKey() val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "air_date") val dateAired: String,
    @ColumnInfo(name = "label") val label: String,
    @ColumnInfo(name = "characters") val characterIds: List<String>,
    @ColumnInfo(name = "time_stamp") val timeStamp: Long
)

@Entity(tableName = "characters")
data class CharacterEntity(
    @PrimaryKey() val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "status") val status: Status?,
    @ColumnInfo(name = "species") val species: String?,
    @ColumnInfo(name = "gender") val gender: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "location") val location: String,
    @ColumnInfo(name = "episodes") val episodesIds: List<String>,
    @ColumnInfo(name = "time_stamp") val timeStamp: Long
)

@Entity(tableName = "favorite_characters")
data class FavoriteCharactersEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "character_id") val characterId: Int,
    @ColumnInfo(name = "is_favorite") val isFavorite: Boolean
)

@Entity(tableName = "favorite_episodes")
data class FavoriteEpisodesEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "episode_id") val episodeId: Int,
    @ColumnInfo(name = "is_favorite") val isFavorite: Boolean
)

fun Character.toEntity() =
    CharacterEntity(id, name, status, species, gender, image, location.name, episodesUrls, System.currentTimeMillis())

fun CharacterEntity.toCharacter() =
    Character(id, name, status, species, gender, image, Location(location), episodesIds, false)

fun Episode.toEntity() = EpisodeEntity(id, name, dateAired, label, characterUrls, System.currentTimeMillis())
fun EpisodeEntity.toEpisode() = Episode(id, name, dateAired, label, null, characterIds, false)

val Any.timeStamp: Long
    get() = when (this) {
        is CharacterEntity -> timeStamp
        is EpisodeEntity -> timeStamp
        else -> -1
    }