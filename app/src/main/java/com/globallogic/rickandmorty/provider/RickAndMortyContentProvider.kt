package com.globallogic.rickandmorty.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import com.globallogic.rickandmorty.common.constants.Constants
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.runBlocking
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

private const val TYPE_ALL_CHARACTERS = 1
private const val TYPE_CHARACTER = 2

class RickAndMortyContentProvider : ContentProvider(), KoinComponent {
    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH).apply {
        addURI(Constants.AUTHORITY, Constants.PATH_CHARACTERS, TYPE_ALL_CHARACTERS)
        addURI(Constants.AUTHORITY, Constants.PATH_CHARACTER, TYPE_CHARACTER)
    }

    private val repository by inject<Repository>()

    override fun onCreate(): Boolean = true

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        val characters = runBlocking {
            when (uriMatcher.match(uri)) {
                TYPE_ALL_CHARACTERS -> repository.getAllCharacters()
                TYPE_CHARACTER -> repository.getCharacters(listOf(uri.lastPathSegment))
                else -> error("")
            }
        }
        val cursor = MatrixCursor(
            arrayOf(
                Constants.Column.ID,
                Constants.Column.NAME,
                Constants.Column.STATUS,
                Constants.Column.IMAGE,
                Constants.Column.GENDER,
                Constants.Column.SPECIES
            )
        )
        characters.forEach { it.insertIntoCursor(cursor) }

        return cursor
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? = null
    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int = 0
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int = 0
    override fun getType(uri: Uri): String? = null
}

fun Character.insertIntoCursor(cursor: MatrixCursor) {
    cursor.addRow(arrayOf(id, name, status?.name, image, gender, species))
}