package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import com.globallogic.rickandmorty.common.extensions.load
import com.globallogic.rickandmorty.common.extensions.trigger
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.CoroutineDispatcher

class FavoriteViewModel(private val repository: Repository, dispatcher: CoroutineDispatcher) : BaseViewModel(dispatcher) {
    val character = MutableLiveData<Character>()
    val characters = MutableLiveData<List<Character>>()
    val episode = MutableLiveData<Episode>()
    val episodes = MutableLiveData<List<Episode>>()

    fun fetchCharacterFavoriteStatus(character: Character) = load(isLoading, error) {
        character.isFavorite = repository.isFavorite(character)
        this@FavoriteViewModel.character.postValue(character)
    }

    fun fetchCharacterFavoriteStatus(charactersList: List<Character>) = load(isLoading, error) {
        charactersList.forEach { it.isFavorite = repository.isFavorite(it) }
        characters.postValue(charactersList)
    }

    fun fetchEpisodeFavoriteStatus(episode: Episode) = load(isLoading, error) {
        episode.isFavorite = repository.isFavorite(episode)
        this@FavoriteViewModel.episode.postValue(episode)
    }

    fun fetchEpisodesFavoriteStatus(episodesList: List<Episode>) = load(isLoading, error) {
        episodesList.forEach { it.isFavorite = repository.isFavorite(it) }
        episodes.postValue(episodesList)
    }

    fun reloadCharacterFavoriteStatus() = load(isLoading, error) {
        characters.value.let {
            it?.forEach { character ->
                character.isFavorite = repository.isFavorite(character)
            }
            characters.trigger()
        }
    }

    fun reloadEpisodeFavoriteStatus() = load(isLoading, error) {
        episodes.value.let {
            it?.forEach { episode ->
                episode.isFavorite = repository.isFavorite(episode)
            }
            episodes.trigger()
        }
    }

    fun setFavorite(item: Any, isFavorite: Boolean) = load(isLoading, error) {
        when (item) {
            is Character -> {
                item.isFavorite = isFavorite
                repository.updateFavoriteStatus(item)
                this@FavoriteViewModel.character.postValue(item)
            }
            is Episode -> {
                item.isFavorite = isFavorite
                repository.updateFavoriteStatus(item)
                this@FavoriteViewModel.episode.postValue(item)
            }
        }
    }
}