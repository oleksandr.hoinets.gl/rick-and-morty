package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.globallogic.rickandmorty.common.extensions.load
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.datasource.CharactersDataSourceFactory
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.CoroutineDispatcher

class CharacterViewModel(private val repository: Repository, dispatcher: CoroutineDispatcher) : BaseViewModel(dispatcher) {
    val characters = MutableLiveData<List<Character>>()

    private val factory = CharactersDataSourceFactory()
    val pagedList: LiveData<PagedList<Character>>
    val loading: LiveData<Boolean> = Transformations.switchMap(factory.dataSource) { it.loading }
    val errorLoading: LiveData<Throwable?> = Transformations.switchMap(factory.dataSource) { it.error }
    val errorLoadingNext: LiveData<Throwable?> = Transformations.switchMap(factory.dataSource) { it.errorLoadingNext }

    init {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .build()

        pagedList = LivePagedListBuilder<Int, Character>(factory, config).build()
    }

    fun loadCharacters(episode: Episode) {
        characters.value ?: reloadCharacters(episode)
    }

    fun reloadCharacters(episode: Episode) = load(isLoading, error) {
        val charactersIds = episode.characterUrls
        characters.postValue(repository.getCharacters(charactersIds))
    }
}