package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.Repository
import com.globallogic.rickandmorty.repository.database.timeStamp
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class DownloadedViewModel(private val repository: Repository, dispatcher: CoroutineDispatcher) : BaseViewModel(dispatcher) {
    val isDownloaded = MutableLiveData<Boolean>()
    val downloads = MutableLiveData<List<Any>>()

    fun fetchAll() = launch {
        val charactersAll = repository.getAllCharacters(true)
        val episodes = repository.getAllEpisodes(true)
        val allEntities = (charactersAll + episodes)
            .sortedBy { it.timeStamp }

        downloads.postValue(allEntities)
    }

    fun fetchDownloadedStatus(character: Character) = launch {
        isDownloaded.postValue(repository.isDownloaded(character))
    }

    fun fetchDownloadedStatus(episode: Episode) = launch {
        isDownloaded.postValue(repository.isDownloaded(episode))
    }

    fun download(character: Character, add: Boolean) = insertOrDelete(isDownloaded, add,
        { repository.insertToDownload(character) },
        { repository.deleteFromDownload(character) })

    fun download(episode: Episode, add: Boolean) = insertOrDelete(isDownloaded, add,
        { repository.insertToDownload(episode) },
        { repository.deleteFromDownload(episode) })

    private fun insertOrDelete(
        isDownloaded: MutableLiveData<Boolean>,
        add: Boolean,
        trueAction: suspend () -> Unit,
        falseAction: suspend () -> Unit
    ) {
        launch {
            try {
                isDownloaded.postValue(add)
                if (add) {
                    trueAction()
                } else {
                    falseAction()
                }
            } catch (e: Exception) {
                isDownloaded.postValue(!add)
            }
        }
    }
}