package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import com.globallogic.rickandmorty.R
import com.globallogic.rickandmorty.common.model.*
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.CoroutineDispatcher

class SearchViewModel(private val repository: Repository, dispatcher: CoroutineDispatcher) : BaseViewModel(dispatcher) {
    val searchResults = MutableLiveData<List<Any>>()

    private var query = ""
    private var isOnline = true

    fun toggleOnline() {
        isOnline = !isOnline
        search(query)
    }

    fun search(query: String) {
        this.query = query

        performSearch(query)
    }

    private fun performSearch(query: String) = load {
        val characters = repository.searchCharacters(query, isOnline)
        val episodes = repository.searchEpisodes(query, isOnline)

        val results = makeResults(characters, episodes, isOnline)

        searchResults.postValue(if (query.isNotBlank()) results else listOf())
    }

    private fun makeResults(characters: List<Character>, episodes: List<Episode>, onlineSearch: Boolean = false)
            : MutableList<Any> {
        val results = mutableListOf<Any>()

        if (characters.isNotEmpty()) {
            results += SearchHeader(R.string.characters)
            results.addAll(characters)
            results += SearchSeparator()
        }

        if (episodes.isNotEmpty()) {
            results += SearchHeader(R.string.episodes)
            results.addAll(episodes)
            results += SearchSeparator()
        }

        if (results.isEmpty()) {
            results += SearchNoResults()
            if (onlineSearch) results += SwitchToSourceButton(R.string.try_online_search)
        } else {
            results += if (!onlineSearch) SwitchToSourceButton(R.string.load_offline_results) else SwitchToSourceButton(
                R.string.load_online_results
            )
        }

        return results
    }
}