package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import com.globallogic.rickandmorty.common.extensions.findRegex
import com.globallogic.rickandmorty.common.extensions.load
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode
import com.globallogic.rickandmorty.repository.Repository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job

class EpisodeViewModel(private val repository: Repository, dispatcher: CoroutineDispatcher) : BaseViewModel(dispatcher) {
    val episodes = MutableLiveData<List<Episode>>()
    val nextPage = MutableLiveData<Int>()
    private var job: Job? = null

    fun loadAllEpisodes() {
        episodes.value ?: loadEpisodes()
    }

    fun loadEpisodes(nextPage: Int = 1) {
        if (nextPage == -1) return
        if (job?.isActive != true) {
            job = load(isLoading, error) {
                isLoading.postValue(false)

                val response = repository.getEpisodes(nextPage)
                this@EpisodeViewModel.nextPage.postValue(response.first)

                episodes.postValue(episodes.value?.let { it + repository.randomCharacters(response.second) }
                    ?: repository.randomCharacters(response.second))
            }
        }
    }

    fun loadEpisodes(character: Character) {
        episodes.value ?: reloadEpisodes(character)
    }

    fun reloadEpisodes(character: Character) = load(isLoading, error) {
        val episodesIds = character.episodesUrls
        episodes.postValue(
            if (episodesIds.size > 1) {
                loadEpisodes(episodesIds)
            } else {
                episodesIds.mapNotNull { it.findRegex("\\d+") }.firstOrNull()?.let { loadEpisode(it) }
            }
        )
    }

    private suspend fun loadEpisodes(episodesIds: List<String>) = repository.getEpisodes(episodesIds)
    private suspend fun loadEpisode(episodeId: String) = listOf(repository.getEpisode(episodeId.toInt()))
}