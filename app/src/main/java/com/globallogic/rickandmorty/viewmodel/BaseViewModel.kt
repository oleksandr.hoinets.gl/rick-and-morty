package com.globallogic.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.globallogic.rickandmorty.common.extensions.load
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(private val dispatcher: CoroutineDispatcher) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = dispatcher

    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable>()
}

inline fun BaseViewModel.load(crossinline action: suspend () -> Unit) =
    load(isLoading, error, action)