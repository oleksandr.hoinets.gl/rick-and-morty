package com.globallogic.rickandmorty.common.model

interface Favorite {
    var isFavorite: Boolean
}