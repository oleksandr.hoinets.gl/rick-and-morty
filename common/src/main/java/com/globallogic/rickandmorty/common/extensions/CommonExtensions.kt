package com.globallogic.rickandmorty.common.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun String.findRegex(regex: String) = regex.toRegex().find(this)?.value

fun String.findAllRegex(regex: String) = regex.toRegex().findAll(this).toList().map { it.value }

val Boolean.asInt: Int get() = if (this) 1 else 0

val Int.asBoolean: Boolean get() = this != 0

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, observer: (T) -> Unit) {
    liveData.observe(this, Observer { observer(it) })
}

fun <T> MutableLiveData<T>.trigger() = value?.let { postValue(it) }

inline fun CoroutineScope.load(
    loading: MutableLiveData<Boolean>,
    error: MutableLiveData<Throwable>,
    crossinline action: suspend () -> Unit
) = launch {
    try {
        loading.postValue(true)
        action()
    } catch (e: Exception) {
        error.postValue(e)
    } finally {
        loading.postValue(false)
    }
}

fun Context.isOnline(): Boolean {
    val connManager: ConnectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo: NetworkInfo? = connManager.activeNetworkInfo

    return networkInfo != null && networkInfo.isConnectedOrConnecting
}
