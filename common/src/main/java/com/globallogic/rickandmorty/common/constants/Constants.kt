package com.globallogic.rickandmorty.common.constants

object Constants {
    const val SCHEME = "content://"
    const val AUTHORITY = "rickandmorty.provider"
    const val PATH_CHARACTERS = "character"
    const val PATH_CHARACTER = "character/#"

    object Column {
        const val ID = "id"
        const val NAME = "name"
        const val IMAGE = "image"
        const val STATUS = "status"
        const val GENDER = "gender"
        const val SPECIES = "species"
    }
}