package com.globallogic.rickandmorty.common.model

import androidx.annotation.StringRes

data class SearchNoResults(private val nothing: Unit = Unit)

data class SearchHeader(@StringRes val res: Int)

data class SearchSeparator(private val nothing: Unit = Unit)

data class SwitchToSourceButton(@StringRes val res: Int)
