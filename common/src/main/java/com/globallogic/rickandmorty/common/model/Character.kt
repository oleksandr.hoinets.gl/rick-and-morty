package com.globallogic.rickandmorty.common.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Character(val id: Int, val name: String, val status: Status?, val species: String?,
                     val gender: String, val image: String, val location: Location,
                     @SerializedName("episode") val episodesUrls: List<String>,
                     override var isFavorite: Boolean
) : Parcelable, Favorite

enum class Status {
    @SerializedName("Alive")
    ALIVE,
    @SerializedName("Dead")
    DEAD,
    @SerializedName("Unknown")
    UNKNOWN
}