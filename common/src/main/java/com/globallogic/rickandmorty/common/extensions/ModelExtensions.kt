package com.globallogic.rickandmorty.common.extensions

import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Episode

val Any.id: Int
    get() = when (this) {
        is Character -> id
        is Episode -> id
        else -> -1
    }

var Any.isFavorite
    get() = when (this) {
        is Character -> isFavorite
        is Episode -> isFavorite
        else -> false
    }
    set(value) {
        when (this) {
            is Character -> isFavorite = value
            is Episode -> isFavorite = value
        }
    }

inline fun <reified T : Any> T.isSameId(item: Any) = if (id != -1 && item is T) id == item.id else false
