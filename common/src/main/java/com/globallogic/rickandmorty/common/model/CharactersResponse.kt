package com.globallogic.rickandmorty.common.model

data class CharactersResponse(val results: List<Character>, val info: Info)