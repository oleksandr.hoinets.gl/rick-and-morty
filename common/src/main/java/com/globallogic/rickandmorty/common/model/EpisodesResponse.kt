package com.globallogic.rickandmorty.common.model

data class EpisodesResponse(val results: List<Episode>, val info: Info)