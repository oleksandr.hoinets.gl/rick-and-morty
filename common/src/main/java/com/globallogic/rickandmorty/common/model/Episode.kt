package com.globallogic.rickandmorty.common.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Episode(val id: Int, val name: String, @SerializedName("air_date") val dateAired: String,
                   @SerializedName("episode") val label: String, @Transient var characters: List<Character>?,
                   @SerializedName("characters") val characterUrls: List<String>,
                   override var isFavorite: Boolean
) : Parcelable, Favorite