package com.globallogic.rickandmorty.library

import android.content.Context
import android.database.Cursor
import android.net.Uri
import com.globallogic.rickandmorty.common.constants.Constants
import com.globallogic.rickandmorty.common.model.Character
import com.globallogic.rickandmorty.common.model.Location
import com.globallogic.rickandmorty.common.model.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RickAndMortyLib(val context: Context) {
    suspend fun getAllCharacters(): List<Character> = withContext(Dispatchers.IO) {

        val cursor = context.contentResolver.query(
            Uri.parse("${Constants.SCHEME}${Constants.AUTHORITY}/${Constants.PATH_CHARACTERS}/"),
            null,
            null,
            null,
            null
        )
        cursor?.convertToCharacter().apply { cursor?.close() } ?: listOf()
    }

    suspend fun getCharacter(id: Int): Character? = withContext(Dispatchers.IO) {

        val cursor = context.contentResolver.query(
            Uri.parse("${Constants.SCHEME}${Constants.AUTHORITY}/${Constants.PATH_CHARACTERS}/$id"),
            null,
            null,
            null,
            null
        )
        (cursor?.convertToCharacter().apply { cursor?.close() } ?: listOf()).first()
    }
}

fun Cursor.convertToCharacter(): List<Character> {
    val characters = mutableListOf<Character>()

    if (moveToFirst()) {
        do {
            characters.add(
                Character(
                    getInt(getColumnIndex(Constants.Column.ID)),
                    getString(getColumnIndex(Constants.Column.NAME)),
                    Status.valueOf(getString(getColumnIndex(Constants.Column.STATUS)) ?: Status.UNKNOWN.name),
                    getString(getColumnIndex(Constants.Column.SPECIES)),
                    getString(getColumnIndex(Constants.Column.GENDER)),
                    getString(getColumnIndex(Constants.Column.IMAGE)),
                    Location("NULL"),
                    listOf(),
                    false
                )
            )
        } while (moveToNext())
    }

    return characters
}